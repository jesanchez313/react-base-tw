import logo from "./logo.svg";

function App() {
  return (
    <div className="bg-green-700 h-screen flex justify-center items-center text-white">
      <header className="App-header">
        <img src={logo} className="w-24" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
